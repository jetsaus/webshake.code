<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Максимальное произведение двух целых чисел</title>
</head>
<body>
<h3>Максимальное произведение двух целых чисел</h3>
<p>На вход подается строка из чисел, разделенных пробелами. Найдите максимальное произведение двух чисел из этой строки.</p>
<h3>Решение:</h3>
<?php
    $strNum = '-10 8 8 11 6 -10 215';                           // Исходная строка
    if (isset($strNum)) {
        echo 'Исходная строка: ' . "<b><i>$strNum</i></b><br>";
        $arrNum = explode(' ', $strNum);                     // Преобразование в массив
        $mulMax = 0;                                        // Произведение максимумов
        for ($i=0;$i<=count($arrNum)-1;$i++) {
            for ($j=$i+1;$j<=count($arrNum)-1;$j++) {
                if ($arrNum[$i]*$arrNum[$j]>$mulMax) {
                    $mulMax=$arrNum[$i]*$arrNum[$j];
                }
            }
        }
        echo 'Максимальное произведение: ' . "<b><i>$mulMax</i></b>";
    }
    
/*  Решение автора задачи
<?php
$line = trim(fgets(STDIN));
$nums = explode(' ', $line);

$max = $nums[0];
// Цикл в цикле для foreach()
foreach($nums as $i => $num1) {
    foreach($nums as $j => $num2) {
        if ($i === $j) {
            continue;
        }
        $product = $num1 * $num2;
        if ($product > $max) {
            $max = $product;
        }
    }
}
echo $max;
*/
?>
</body>
</html>
