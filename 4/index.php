<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Числа Фибоначчи</title>
</head>
<body>
<h3>Числа Фибоначчи</h3>
<p>Числа Фибоначчи - это последовательность чисел, в которой первые два числа это 0 и 1, а каждое следующее число равно сумме двух предыдущих.<br>
    В этой задаче вам нужно написать код, который будет выводить первые n чисел этой последовательности.</p>

<h3>Решение:</h3>
<?php
    $N = 7;
    $F=[];
    echo "N = <b><i>$N</i></b><br>";
    echo 'Числа Фибоначчи от N: <b><i>';
    if ($N>1) {                            // Ограничим положительными 0, 1
        // Заполним массив числами Фибоначчи
        for ($i=0;$i<=($N-1);$i++) {
            if ($i<=1) {
                $F[]=$i;
            } else {
                $F[]=$F[$i-2]+$F[$i-1];
            }
        }
        // Отобразим массив
        echo '<b><i>';
        foreach ($F as $fib) {
            echo $fib . ' ';
        }
        echo '</i></b>';
    }
    
    

?>
</body>
</html>
