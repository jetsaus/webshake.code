<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Уникальные числа</title>
</head>
<body>
<h3>Уникальные числа</h3>
<p>На вход дана строка с числами, разделенными пробелами.
    Удалите все повторы чисел. Выведите их в любом порядке, разделив пробелами.</p>

<h3>Решение:</h3>
<?php
    $strNumber = '0 1 1 2 3 3 3 4 5 5 5 6 7 7 7 7 7 2 7 0';
    $arrNumber = explode(' ', $strNumber);
    $arrUnique = array_unique($arrNumber);
    $strUnique = implode(' ', $arrUnique);
    echo 'Исходная стока: <b><i>' . $strNumber . '</i></b><br>';
    echo 'Уникальные числа: <b><i>' . $strUnique . '</i></b>';
?>
</body>
</html>
