<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Чётные числа</title>
</head>
<body>
<h3>Чётные числа</h3>
<p>Дана строка с числами, разделенными пробелом. Найдите все четные числа и выведите их, разделяя пробелами. Порядок чисел должен быть таким же, как и на входе.</p>

<h3>Решение:</h3>
<?php
    $strNum = '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25';
    $arrNum = explode(' ', $strNum);    // создание массива из строки
    echo 'Исходная строка: ' . "<b><i>$strNum</i></b><br>";
    echo 'Четные числа: <b><i>';
    foreach($arrNum as $num) {          // пробежим по всем элементам массива
        if(intval($num)%2==0) {
            echo $num . ' ';
        }
    }
    echo '</i></b>';
    

?>
</body>
</html>
