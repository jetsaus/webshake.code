<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Наиболее часто встречающееся число</title>
</head>
<body>
<h3>Наиболее часто встречающееся число</h3>
<p>На вход подается строка из чисел, разделенных пробелами. Найдите наиболее часто встречающееся число в строке.</p>
<h3>Решение:</h3>
<?php
    $strNum = '-10 7 8 7  7 8 7 7 11 6 -10 7 8 -10 7 11 7 215 7';    // Исходная строка
    if (isset($strNum)) {
        echo 'Исходная строка: ' . "<b><i>$strNum</i></b><br>";
        $arrNum = explode(' ', $strNum);                        // Преобразование в массив
        $countNum = array_count_values($arrNum);                // Подсчет повторений
        $mostNum = array_search(max($countNum), $countNum);     // Наиболее частое число
        $count = max($countNum);                                // Количество вхождений числа
        echo 'Наиболее часто встречающееся число ' . "<b><i>$mostNum</i></b>, ";
        echo 'встречается ' . "<b><i>$count</i></b> раз";
    }
    
/**
 *      Решение автора
 *
<?php
    $line = trim(fgets(STDIN));
    $nums = explode(' ', $line);
    $nums2Freq = [];
    $currentMaxFreq = 1;
    $currentMaxNum = $nums[0];
    foreach ($nums as $num) {
        if (!isset($nums2Freq[$num])) {
            $nums2Freq[$num]=1;
        } else {
            $nums2Freq[$num]++;
        }
        if ($nums2Freq[$num] > $currentMaxFreq) {
            $currentMaxFreq = $nums2Freq[$num];
            $currentMaxNum = $num;
        }
    }
    echo $currentMaxNum;
 */
?>
</body>
</html>
