<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Здравствуй, Мир!</title>
</head>
<body>
<h3>Привет, мир!</h3>
<p>Написать программу, которая строку переведет в UPPERCASE (ВЕРХНИЙ РЕГИСТР).</p>

<h3>Решение:</h3>
<?php
    $str = 'привет, мир!';
    echo '<p><b><i>' . mb_strtoupper($str) . '</i></b>';
?>
</body>
</html>
