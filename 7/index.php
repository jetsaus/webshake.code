<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Повторяющиеся значения</title>
</head>
<body>
<h3>Повторяющиеся значения</h3>
<p>На вход подается строка из чисел, разделенных пробелами. Найдите все числа, встречающиеся 2 и более раз. Выведите их в любом порядке, разделяя пробелами.</p>
<h3>Решение:</h3>
<?php
    $strNum = '18 1 3 18 5 76 5 18 37 28 89 11 3 5';        // Исходная строка
    echo 'Исходная строка: ' . "<b><i>$strNum</i></b><br>";
    $arrNum = explode(' ', $strNum);                    // Преобразование в массив
    sort($arrNum);                                      // Сортировка
    $assNum = array_count_values($arrNum);              // Подсчет повторений элементов -> в массив
    $repNum = '';                                       // Повторяющиеся числа
    foreach ($assNum as $num => $countNum) {            // Пробежим по массиву
        if ($countNum>1) {                              // Количество повторений больше 1
            $repNum .= $num;                            // Добавим повторяющееся число
            $repNum .= ' ';                             // Добавим пробел после числа
            
        }
    }
    echo 'Результирующая строка: ' . "<b><i>$repNum</i></b>";
/*  Решение автора задачи
<?php
$line = trim(fgets(STDIN));
$nums = explode(' ', $line);

$duplicatedValues2Count = [];
foreach ($nums as $num) {
    if (!isset($duplicatedValues2Count[$num])) {
        $duplicatedValues2Count[$num] = 1;
    } else {
        $duplicatedValues2Count[$num]++;
    }

    if ($duplicatedValues2Count[$num] === 2) {
        echo $num . ' ';
    }
}
*/
?>
</body>
</html>
