<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Анаграммы</title>
</head>
<body>
<h3>Анаграммы</h3>
<p>Одна строка является анаграммой другой стро и, если из первой можно получить вторую путём перестановки символов. Например, строки 'heart' и 'earth' - анаграммы. Строки 'python' и 'typhon' - тоже.
В этой задаче вам нужно написать код, который получит на вход две строки из и выведет 'Да' если они являются анаграммами друг друга, и 'Нет' если не являются.</p>
<h3>Решение:</h3>
<?php
    $strOne = 'Спаниель';
    $strTwo = 'Апельсин';
    echo 'Первое слово: ' . "<b><i>$strOne</i></b><br>";
    echo 'Второе слово: ' . "<b><i>$strTwo</i></b><br><br>";
    echo 'Это анаграмма?: ';
    // Проверка равенства длин слов
    if(strlen($strOne)!=strlen($strTwo)) {
        echo '<b><i>Нет</i></b>';
        die;
    }
    // Перевод слов верхний регистр
    $strOne = mb_strtoupper($strOne);
    $strTwo = mb_strtoupper($strTwo);
    // Преобразование строк в массивы
    $arrStrOne = preg_split('//u', $strOne);
    $arrStrTwo = preg_split('//u', $strTwo);
    // Сортировка по алфавиту
    sort($arrStrOne);
    sort($arrStrTwo);
    // Посимвольное сравнение
    for ($i=0;$i<=(count($arrStrOne)-1);$i++) {
        if ($arrStrOne[$i]!==$arrStrTwo[$i]) {
            echo '<b><i>Нет</i></b>';
            die;
        }
    }
    echo '<b><i>Да</i></b>';
?>
</body>
</html>
